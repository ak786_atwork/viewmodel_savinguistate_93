package com.example.anil.viewmodel_savinguistate_93;

import android.arch.lifecycle.ViewModel;

public class ViewModelDemo extends ViewModel {

    private String data = "";

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
