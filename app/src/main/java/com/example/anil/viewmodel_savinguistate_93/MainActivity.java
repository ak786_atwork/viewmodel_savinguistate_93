package com.example.anil.viewmodel_savinguistate_93;

import android.arch.lifecycle.ViewModel;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private ViewModelDemo viewModel;
    private EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.text);
        viewModel = ViewModelProviders.of(this).get(ViewModelDemo.class);

        if (viewModel.getData() != "")
        {
            editText.setText(viewModel.getData());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        viewModel.setData(editText.getText().toString());
    }
}
